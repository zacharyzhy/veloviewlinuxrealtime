TARGET=veloviewer
LIB=-lglut -lGLU -lrt
newcontrol: 
	g++ veloviewer.cpp SensorFunctions.cpp SensorFunctions.h -o $(TARGET) $(LIB)

newcontrolClang:
	clang -x c++ veloviewer.cpp SensorFunctions.cpp SensorFunctions.h -o $(TARGET) $(LIB)

clean:
	rm *.o $(TARGET)  *~ *.out *.temp
