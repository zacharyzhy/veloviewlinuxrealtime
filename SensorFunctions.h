#ifndef _SENSOR_INCLUDE_
#define _SENSOR_INCLUDE_

#include <cstdio>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>
#include <sys/poll.h>
#include <cctype>
#include <cmath>
#include <cstdlib>
#include <ctime>

#define NUM_POINTS 80000
extern int sockfd_;
extern unsigned int packet_size;
extern int udp_port;
extern bool playback;
extern FILE* pfile;
extern int pointCount;
extern float intensity[NUM_POINTS], x[NUM_POINTS], y[NUM_POINTS], z[NUM_POINTS];
struct laserconf
{
	//uint8_t number;
	double vertCorrection;
	double rotCorrection;
	double farCorrection;
	double xCorrection;
	double yCorrection;
	double vOffCorrection;
	double hOffCorrection;
	double focalDist;
	double focalSlope;
	uint8_t minIntensity;
	uint8_t maxIntensity;
};


struct blockconf
{
	laserconf laser[32];
	char title[5];
};

struct conf
{
	blockconf highBlock;
	blockconf lowBlock;
	int motorSpeed;
};
extern conf configuration;
struct point
{
	double distance;
	uint8_t intensity;
};
struct laserblock
{
	uint16_t id;
	double rotation;
	point data[32];
};
struct packet
{
	laserblock block[12];
	uint32_t time;
	uint8_t statType;
	uint8_t statVal;
};

//functions list:
//descriptions of functions available in SensorFunctions.cpp
double normalize_angle(double angle);
double from_degrees(double degrees);
int parseBuffer(uint8_t* buffer, packet* pack);
void printPacket(packet pack);
int vopen(void);
int getPackets(uint8_t *buffer, int npacks);
int read1Status(char* type, double* value);
int readConfiguration(conf* config);
void printLaserConfig(const blockconf* block, int lasernum);
int processPacket(const packet* pkt, conf* cfg);

#endif
