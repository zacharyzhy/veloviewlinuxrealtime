#include <stdio.h>
#include <GL/glut.h>
#include "SensorFunctions.h"
#include <ctime>
#include <math.h>
#include <iostream>
const int   TEXT_WIDTH      = 8;
const int   TEXT_HEIGHT     = 13;
void *font = GLUT_BITMAP_8_BY_13;
const int LIDAR_TILT_COMPENSATION_ANGLE = -5;
using namespace std;
#define window_width  800
#define window_height 600

// display mode 
enum DISPLAY_MODE {BIRDS_EYE, FIRST_PERSON};
int dispMode = BIRDS_EYE;

// initial camera position
float cam_pos_x = 0, cam_pos_y = 0, cam_pos_z = -20; 
float cam_rot_x = 0, cam_rot_y = 0, cam_rot_z = 0;

// camera movement
int rotCam = 0, moveCam = 0, moveZCam = 0;
int camLastX = 0, camLastY = 0, camLastZ = 0;

// Arrow movement parameters
int levelMovement = 0;
int verticalMovement = 0;
int zoom = 0;
// BIRDS_EYE view parameters
float augmentedRotX = 0;
float augmentedRotZ = 0;

// FIRST_PERSON view parameters
int lookAroundAngle = 0;

// color style
enum COLORING_MODE {BY_DISTANCE, BY_HEIGHT, NO_COLOR};
int colorMode = BY_DISTANCE;

float intensity[NUM_POINTS], x[NUM_POINTS], y[NUM_POINTS], z[NUM_POINTS];
int pointCount =0;
float groundFrame[9] = {0.9999800441981136, 0.00037833642524528284, 0.0036987820702358474,
                        0.00037833642524528284, 0.9896323094339619, -0.10125933784433956,
                        -0.003718152332877361, 0.10179006768396229, 0.9947925857075474
                       };

void GL_Setup(int width, int height)
{

        glViewport( 0, 0, width, height );
        glMatrixMode( GL_PROJECTION );
        glEnable( GL_DEPTH_TEST );
        gluPerspective( 45, (float)width/height, .1, 100 );
        glMatrixMode( GL_MODELVIEW );
}


void GL_DispInit() {
       /* white ambient light at half intensity (rgba) */
    GLfloat LightAmbient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
 
    /* super bright, full intensity diffuse light. */
    GLfloat LightDiffuse[] = { .60f, .60f, .60f, 1.0f };
    //GLfloat LightDiffuse[] = { .80f, .50f, .50f, 1.0f };
 
    /* super bright, full intensity specular light. */
    GLfloat LightSpecular[] = {.8, .8, .08, 1.0};
 
    /* position of light (x, y, z, (position of light)) */
    GLfloat LightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
 
 
    glShadeModel(GL_SMOOTH);                                                        // Enable Smooth Shading
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);                                           // Black Background
    glClearDepth(1.0f);                                                             // Depth Buffer Setup
    glEnable(GL_DEPTH_TEST);                                                        // Enables Depth Testing
    glDepthFunc(GL_LEQUAL);                                                         // The Type Of Depth Testing To Do
    glEnable ( GL_COLOR_MATERIAL );
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void NormalKeyControl(unsigned char key, int x, int y) {
    switch(key) {
    case 27: // Esc
    case 'h':
        // home
	dispMode = BIRDS_EYE;
        cam_pos_x = 0;
        cam_pos_y = 0;
        cam_pos_z = -20;
        cam_rot_x = 0;
        cam_rot_y = 0;
        cam_rot_z = 0;
        rotCam = 0;
        moveCam = 0;
        moveZCam = 0;
        levelMovement =0;
        verticalMovement = 0;
        zoom = 0;
        augmentedRotX = 0;
        augmentedRotZ = 0;
        lookAroundAngle = 0;
        
        
        break;
    case 'c':
        colorMode = (colorMode+1)%3;
        break;
    case 'g':
	dispMode = FIRST_PERSON;
        // set view at the LIDAR 
        cam_pos_x = 0;
        cam_pos_y = 0;
        cam_pos_z = 0;
        cam_rot_x = 0;     
        cam_rot_y = -90;
        cam_rot_z = -90; //or -90
        break;
    case 'x':         
        if (zoom <-15)
            zoom -=10;
        else
            zoom -=1;
        break;
    case 'z':
        if (zoom <-15)
            zoom +=10;
        else
            zoom +=1;
        break;
    case 'w': // go forward
	    cam_pos_z += 3;
        break;
    case 's': // go backward
        cam_pos_z -= 3;
        break;
    case 'a':
        if(dispMode == FIRST_PERSON) {
	        lookAroundAngle -= 10;
	    } else {
	        cam_rot_z += 10;    
	    }
        break;
    case 'd':
        if(dispMode == FIRST_PERSON) {
	        lookAroundAngle += 10;
	    } else {
	        cam_rot_z -= 10;    
	    }
        break;
    case 'i':
        if(dispMode == BIRDS_EYE) {
            augmentedRotX -= 3;
        }
        break;
    case 'k':
        if(dispMode == BIRDS_EYE) {
            augmentedRotX += 3;
        }
        break;
    case 'j':
        if(dispMode == BIRDS_EYE) {
            augmentedRotZ += 3;
        }
        break;
    case 'l':
        if(dispMode == BIRDS_EYE) {
            augmentedRotZ -= 3;
        }
        break;
    default:
        break;
    }
}


void ArrowKeysMoveImage(int skey, int x, int y) {
    switch(skey) {
    case GLUT_KEY_UP:
        verticalMovement -= 1.0;
        // cam_pos_y += 1.0;
        break;
    case GLUT_KEY_DOWN:
        verticalMovement += 1.0;
        // cam_pos_y -= 1.0;
        break;
    case GLUT_KEY_LEFT:
        levelMovement += 1.0;
        // cam_pos_x += 1.0;
        break;
    case GLUT_KEY_RIGHT:
        levelMovement -= 1.0;
        // cam_pos_x -= 1.0;
        break;
    default:
        break;
    }
}

void MouseKeyAction(int button, int state, int x, int y) {
    static int button1state = 1;
    static int button2state = 1;
    switch(button) {
    case GLUT_LEFT_BUTTON:
        rotCam = !state;
        button1state = state;
        break;
    case GLUT_RIGHT_BUTTON:
        moveCam = !state;
        button2state = state;
        break;
    case GLUT_MIDDLE_BUTTON:
        moveZCam = !state;
        break;
    default:
        break;
    }
 
    if ((button1state+button2state) == 0) {
        moveZCam = 1;
        rotCam = 0;
        moveCam = 0;
    } else {
        moveZCam = 0;
    }
 
    if(rotCam || moveCam || moveZCam) {
        camLastX = x;
        camLastY = y;
    }
}
 
void MouseMotion(int x, int y) {
    int diffX = x-camLastX, diffY = y-camLastY;
 
    if(rotCam) {
        if (dispMode == BIRDS_EYE) {
            // cam_rot_y += (float)diffX/5.0;
            // cam_rot_x += (float)diffY/5.0;
            // camLastX = x;
            // camLastY = y;
            augmentedRotZ += (float)diffX/5.0;
            
        } else if (dispMode == FIRST_PERSON) {
            lookAroundAngle += (float)diffX/5.0;
        }
        camLastX = x;
        camLastY = y;
    }
    if(moveCam) {
        cam_pos_x += (float)diffX/10.0;
        cam_pos_y -= (float)diffY/10.0;
        camLastX = x;
        camLastY = y;
    }
    if(moveZCam) {
        // cam_pos_z += (float)diffX/10.0;
        // cam_pos_z += (float)diffY/10.0;
        zoom += (float)(diffX+diffY)/10.0;
        camLastX = x;
        camLastY = y;
    }
}

// call back function for window reshape
// it is also called the first time the window is rendered
void WindowReshape(int w, int h) {
    static bool shaped = false;
 
    if (shaped)
        return;
    else
        shaped = true;
 
    glViewport     ( 0, 0, w, h );
    glMatrixMode   ( GL_PROJECTION );  // Select The Projection Matrix
    glLoadIdentity ( );                // Reset The Projection Matrix
 
    gluPerspective ( 60, ( float ) w / ( float ) h, 1.0, 5000.0 );
 
    glMatrixMode   ( GL_MODELVIEW );  // Select The Model View Matrix
    glLoadIdentity ( );    // Reset The Model View Matrix
 
}

void drawString(const char *str, int x, int y, float color[4], void *font)
{
    glPushAttrib(GL_LIGHTING_BIT | GL_CURRENT_BIT); // lighting and color mask
    glDisable(GL_LIGHTING);     // need to disable lighting for proper text color
    glDisable(GL_TEXTURE_2D);

    glColor4fv(color);          // set text color
    glRasterPos2i(x, y);        // place text position

    // loop all characters in the string
    while(*str)
    {
        glutBitmapCharacter(font, *str);
        ++str;
    }

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    glPopAttrib();
}

void drawPrism(float x, float z, float height, float sizebot, float sizetop)
{
    glBegin(GL_LINE_LOOP);
    //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    float alt = 0;
    glColor3f(0.5, 0.5, 0.5);      
    glVertex3f(sizetop,sizetop,alt+height);
    glVertex3f(sizetop,-sizetop,alt+height);
    glVertex3f(-sizetop,sizetop,alt+height);
    glVertex3f(-sizetop,-sizetop,alt+height);
    
    glColor3f(0.4, 0.4, 0.4);      
    glVertex3f(sizebot,-sizebot,alt);
    glVertex3f(-sizebot,-sizebot,alt);
    glVertex3f(sizetop,-sizetop,alt+height);
    glVertex3f(-sizetop,-sizetop,alt+height);
    
    glColor3f(0.5, 0.5, 0.5);      
    glVertex3f(-sizebot,sizebot,alt);
    glVertex3f(sizebot,sizebot,alt);
    glVertex3f(-sizetop,sizetop,alt+height);
    glVertex3f(sizetop,sizetop,alt+height);
    
    glColor3f(0.4, 0.4, 0.4);      
    glVertex3f(sizebot,-sizebot,alt);
    glVertex3f(sizebot,sizebot,alt);
    glVertex3f(sizetop,-sizetop,alt+height);
    glVertex3f(sizetop,sizetop,alt+height);
     
    glColor3f(0.5, 0.5, 0.5);      
    glVertex3f(-sizebot,sizebot,alt);
    glVertex3f(-sizebot,-sizebot,alt);
    glVertex3f(-sizetop,sizetop,alt+height);
    glVertex3f(-sizetop,-sizetop,alt+height);
    
    glEnd();
}

void drawBox(float x, float z, float height, float length, float width)
{
    glBegin(GL_LINE_LOOP);

    glColor3f(0.5, 0.5, 0.5);      
    glVertex3f(x - width/2, z - length/2,0);
    glVertex3f(x - width/2, z + length/2,0);
    glVertex3f(x - width/2, z + length/2,height);
    glVertex3f(x - width/2, z -length/2,height);
    
    glColor3f(0.4, 0.4, 0.4);      
    glVertex3f(x - width/2, z + length/2,0);
    glVertex3f(x + width/2, z + length/2,0);
    glVertex3f(x + width/2, z + length/2,height);
    glVertex3f(x - width/2, z + length/2,height);
    
    glColor3f(0.5, 0.5, 0.5);      
    glVertex3f(x + width/2, z + length/2,0);
    glVertex3f(x + width/2, z - length/2,0);
    glVertex3f(x + width/2, z - length/2,height);
    glVertex3f(x + width/2, z + length/2,height);
    
    glColor3f(0.4, 0.4, 0.4);      
    glVertex3f(x + width/2, z - length/2,0);
    glVertex3f(x - width/2, z - length/2,0);
    glVertex3f(x - width/2, z - length/2,height);
    glVertex3f(x + width/2, z - length/2,height);
    
    glColor3f(0.5, 0.5, 0.5);      
    glVertex3f(x - width/2, z - length/2,height);
    glVertex3f(x - width/2, z + length/2,height);
    glVertex3f(x + width/2, z + length/2,height);
    glVertex3f(x + width/2, z - length/2,height);
    
    glEnd();
}


// GLUT Main loop
void main_loop_function()
{

    static timespec tm;
    long lastm = tm.tv_nsec;
    clock_gettime(CLOCK_REALTIME,&tm);
    int fps = 1.0 * 1000000000 /(tm.tv_nsec - lastm);
    char fpsstring[10];
    sprintf(fpsstring, "FPS: %i", fps);
    // Z angle
    static float angle;

    // Clear color (screen)
    // And depth (used internally to block obstructed objects)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // backup current model-view matrix
    glPushMatrix();                     // save current modelview matrix
    glLoadIdentity();                   // reset modelview matrix

    // set to 2D orthogonal projection
    glMatrixMode(GL_PROJECTION);        // switch to projection matrix
    glPushMatrix();                     // save current projection matrix
    glLoadIdentity();                   // reset projection matrix
    gluOrtho2D(0, window_width, 0, window_height); // set to orthogonal projection

    float color[4] = {1, 1, 1, 1};
    drawString(fpsstring, 1, window_height-TEXT_HEIGHT, color, font);

    // restore projection matrix
    glPopMatrix();                   // restore to previous projection matrix

    // restore modelview matrix
    glMatrixMode(GL_MODELVIEW);      // switch to modelview matrix
    glPopMatrix();

    drawPrism(0,0,.45,.3,.45);
    // lidar -- ground tilt angle compensation
    glRotatef(LIDAR_TILT_COMPENSATION_ANGLE, 1,0,0);
    drawBox(0,.6,-1.3,3.6,1.7); 

    // Load identity matrix
    glLoadIdentity();

    // Multiply in translation matrix
    glTranslatef(cam_pos_x, cam_pos_y, cam_pos_z);
    glTranslatef(levelMovement, verticalMovement, zoom);

    if (dispMode == BIRDS_EYE) {
        glRotatef(augmentedRotX, 1,0,0);
        glRotatef(augmentedRotZ, 0,0,1);
    }
   
    if(dispMode==FIRST_PERSON) {     
        glRotatef(lookAroundAngle, 0,1,0);
    } 
    // Multiply in rotation matrix
    glRotatef(cam_rot_z, 0, 0, 1);
    glRotatef(cam_rot_y, 0, 1, 0);
    glRotatef(cam_rot_x, 1, 0, 0);

    // lidar -- ground tilt angle compensation
    glRotatef(-LIDAR_TILT_COMPENSATION_ANGLE, 1,0,0); 

    // Define Point Size
    glPointSize(1.0);
    // Render colored quad
    glBegin(GL_POINTS);

    // display lidar scan
    for(int i = 0; i<pointCount; ++i)
    {
        float tempx = -y[i]*groundFrame[0]+x[i]*groundFrame[3]
                      +z[i]*groundFrame[6];
        float tempy = -y[i]*groundFrame[1]+x[i]*groundFrame[4]
                      +z[i]*groundFrame[7];
        float tempz = -y[i]*groundFrame[2]+x[i]*groundFrame[5]
                      +z[i]*groundFrame[8];
        if(tempz>-1.3) { // delete the ground
            float t_r = 1, t_g = 1, t_b = 1;
            switch (colorMode) {
                case NO_COLOR:
                    break;
                case BY_DISTANCE: 
                {   // projected distance
                    float _dist = sqrt(tempx*tempx + tempy*tempy);
                    float _distMod9 = fmod(_dist, 9)/9;
                    if (_distMod9 > 0 && _distMod9 <= .2) {
                        t_r = -_distMod9 + 1.0;
                        t_g = _distMod9;
                        t_b = _distMod9;
                    } else if(_distMod9 > .2 && _distMod9 <= .4) {
                        t_r = -_distMod9 + 1.2;
                        t_g = -_distMod9 + 1.2;
                        t_b = _distMod9 - .2;
                    } else if(_distMod9 > .4 && _distMod9 <= .6) {
                        t_r = _distMod9 - .4;
                        t_g = _distMod9 + .4;
                        t_b = _distMod9 - .4;
                    } else if(_distMod9 > .6 && _distMod9 <= .8) {
                        t_r = _distMod9 - .6;
                        t_g = _distMod9 + .2;
                        t_b = _distMod9 + .2;
                    } else if(_distMod9 > .8 && _distMod9 <= 1) {
                        t_r = _distMod9 - .8;
                        t_g = _distMod9 - .8;
                        t_b = _distMod9;
                    } else {
                        t_r = 0;
                        t_g = 1;
                        t_b = 0;
                    }
                    break;
                }
                case BY_HEIGHT:
                {
                    float tempzMod1 = fmod(tempz,1);
                    if (tempzMod1 > 0 && tempzMod1 <= .2) {
                        t_r = -tempzMod1 + 1.0;
                        t_g = tempzMod1;
                        t_b = tempzMod1;
                    } else if(tempzMod1 > .2 && tempzMod1 <= .4) {
                        t_r = -tempzMod1 + 1.2;
                        t_g = -tempzMod1 + 1.2;
                        t_b = tempzMod1 - .2;
                    } else if(tempzMod1 > .4 && tempzMod1 <= .6) {
                        t_r = tempzMod1 - .4;
                        t_g = tempzMod1 + .4;
                        t_b = tempzMod1 - .4;
                    } else if(tempzMod1 > .6 && tempzMod1 <= .8) {
                        t_r = tempzMod1 - .6;
                        t_g = tempzMod1 + .2;
                        t_b = tempzMod1 + .2;
                    } else if(tempzMod1 > .8 && tempzMod1 <= 1) {
                        t_r = tempzMod1 - .8;
                        t_g = tempzMod1 - .8;
                        t_b = tempzMod1;
                    } else {
                        t_r = 0;
                        t_g = 1;
                        t_b = 0;
                    }
                    
                    break;
                }
                default:
                    break;
            }
            glColor3f(t_r,t_g,t_b);
            glVertex3f(tempx,tempy,tempz);
        }
    }
    glEnd();
    // Swap buffers (color buffers, makes previous render visible)
    glutSwapBuffers();
    pointCount = 0;
    fclose(pfile);
    playback =0;
    FILE* rfile = fopen("dumpfile.temp", "w+");
    uint8_t buffer[1207];
    buffer[1206]='\0';
    for(int i = 0; i<335; ++i)
        if(getPackets(buffer,1)!=-1) //read packets
            fwrite(buffer,1,1206,rfile); //write raw data
    pfile =rfile;
    rewind(pfile);
    //ready = (pfile!=NULL); //make sure file was able to be opened
    playback = 1;

    for(int p=0; p<335; ++p) //read 1000 packets of 12 blocks of 32 lasers
    {
        uint8_t buffer[1206];
        packet read1;
        if(getPackets(buffer,1)!=-1) //read in packets
        {
            parseBuffer(buffer, &read1); //parse packets
            processPacket(&read1,&configuration); //process
        }
    }
}


int main(int argc, char** argv)
{
    int ready =(vopen()==0); //make sure UDP socket is opened correctly
    FILE* rfile = fopen("dumpfile.temp", "w+");
    if(ready)
    {
        uint8_t buffer[1207];
        buffer[1206]='\0';
        for(int i = 0; i<10000; ++i)
            if(getPackets(buffer,1)!=-1) //read packets
                fwrite(buffer,1,1206,rfile); //write raw data
    }
    else return 0;
    
    pfile =rfile;
    rewind(pfile);
    ready = (pfile!=NULL); //make sure file was able to be opened
    playback = 1;
    if(ready)
    {
        printf("reading configuration...\n");
        if(readConfiguration(&configuration)!=-1) //make sure read was sucessful
        {
            //FILE* dfile = fopen(dfilename, "w");
            printf("done configuring. \n");
            //START GLUT HERE
            fclose(pfile);
            playback =0;
            FILE* rfile = fopen("dumpfile.temp", "w+");
            uint8_t buffer[1207];
            buffer[1206]='\0';
            for(int i = 0; i<335; ++i)
                if(getPackets(buffer,1)!=-1) //read packets
                    fwrite(buffer,1,1206,rfile); //write raw data
            pfile =rfile;
            rewind(pfile);
            //ready = (pfile!=NULL); //make sure file was able to be opened
            playback = 1;

            for(int x=0; x<335; ++x) //read 1000 packets of 12 blocks of 32 lasers
            {
                uint8_t buffer[1206];
                packet read1;
                if(getPackets(buffer,1)!=-1) //read in packets
                {
                    parseBuffer(buffer, &read1); //parse packets
                    processPacket(&read1,&configuration); // set x,y,z arrays of points
                }
            }
            //END GLUT STUFF
        }
        else 
            return 0;
    }
    else 
        return 0;
    
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);
    glutInitWindowSize(window_width, window_height);
    glutCreateWindow("Velodyne Viewer");

    // glutReshapeFunc(WindowReshape);
    // glutSpecialFunc(arrowKeys);
    glutSpecialFunc(ArrowKeysMoveImage);
    // glutKeyboardFunc(processNormalKeys);
    glutKeyboardFunc(NormalKeyControl);
    glutMouseFunc(MouseKeyAction);
    glutMotionFunc(MouseMotion);

    glutIdleFunc(main_loop_function);

    // GL_Setup(window_width, window_height);
    //GL_DispInit();
    GL_Setup(window_width, window_height);
    glutMainLoop();
}



